package tn.suptech.firstapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirstAppFromEclipseApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstAppFromEclipseApplication.class, args);
	}

}
